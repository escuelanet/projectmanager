﻿using EscuelaNet.Aplicacion.Programadores.Commands;
using EscuelaNet.Aplicacion.Programadores.Responds;
using EscuelaNet.Dominio.Programadores;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Programadores.CommandsHandlers
{
    public class NuevoSkillCommandHandler : IRequestHandler<NuevoSkillCommand, CommandRespond>
    {
        private ISkillRepository _skillRepositorio;
        public NuevoSkillCommandHandler(ISkillRepository skillRepositorio)
        {
            _skillRepositorio = skillRepositorio;
        }
        public Task<CommandRespond> Handle(NuevoSkillCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.Descripcion))
            {
                try
                {
                    var conocimiento = new Skills(request.Descripcion, request.Grados);
                    _skillRepositorio.Add(conocimiento);
                    _skillRepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);
                }
                catch (Exception e)
                {
                    responde.Succes = false;
                    responde.Error = e.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Texto vacio";
                return Task.FromResult(responde);
            }

        }
    }
}
