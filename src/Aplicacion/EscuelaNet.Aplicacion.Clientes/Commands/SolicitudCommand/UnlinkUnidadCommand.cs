﻿using EscuelaNet.Aplicacion.Clientes.Responds;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.Commands.SolicitudCommand
{
    public class UnlinkUnidadCommand : IRequest<CommandRespond>
    {
        public int IDUnidad { get; set; }

        public int IDSolicitud { get; set; }

    }
}
