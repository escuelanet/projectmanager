﻿using EscuelaNet.Aplicacion.Clientes.Responds;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Clientes.Commands.UnidadCommand
{
    public class EditUnidadCommand : IRequest<CommandRespond>
    {
        public int IdCliente { get; set; }

        public int IdUnidad { get; set; }

        public string RazonSocial { get; set; }

        public string ResponsableDeUnidad { get; set; }

        public string Cuit { get; set; }

        public string EmailResponsable { get; set; }

        public string TelefonoResponsable { get; set; }

    }
}
