﻿using EscuelaNet.Aplicacion.Conocimiento.Command;
using EscuelaNet.Aplicacion.Conocimiento.Command.ConocimientoCommand;
using EscuelaNet.Aplicacion.Conocimiento.Responds;
using EscuelaNet.Dominio.Conocimientos;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.CommandHandlers.ConocimientoCommandHanlders
{
    public class NewConocimientoCommandHandlers : IRequestHandler<NewConocimientoCommand, CommandRespond>
    {
        private ICategoriaRepository _categoriarepositorio;

        public NewConocimientoCommandHandlers(ICategoriaRepository categoriarepositorio)
        {
            _categoriarepositorio = categoriarepositorio;
        }

        public Task<CommandRespond> Handle(NewConocimientoCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            if (!string.IsNullOrEmpty(request.Nombre))

            {
                try
                {
                    var nombre = request.Nombre;

                    var categoria = _categoriarepositorio.GetCategoria(request.IdCate);

                    var conocimiento = new EscuelaNet.Dominio.Conocimientos.Conocimiento(nombre);

                    categoria.AgregarConocimiento(conocimiento);
                    _categoriarepositorio.Update(categoria);
                    _categoriarepositorio.UnitOfWork.SaveChanges();

                    responde.Succes = true;
                    return Task.FromResult(responde);

                }
                catch (Exception ex)
                {
                    responde.Succes = false;
                    responde.Error = ex.Message;
                    return Task.FromResult(responde);
                }
            }
            else
            {
                responde.Succes = false;
                responde.Error = "Complete todos los campos, por favor";
                return Task.FromResult(responde);
            }
        }
    }
}
