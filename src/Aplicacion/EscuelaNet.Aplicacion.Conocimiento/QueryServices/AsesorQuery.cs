﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using EscuelaNet.Aplicacion.Conocimiento.QueryModels;

namespace EscuelaNet.Aplicacion.Conocimiento.QueryServices
{
    public class AsesorQuery : IAsesorQuery
    {
        private string _connectionString;
        public AsesorQuery(string connectionString)
        {
            _connectionString = connectionString;
        }

        public AsesorQueryModel EncontrarConocimientoAsesor(int id, int asesor)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection.Query<AsesorQueryModel>(
                    @"SELECT c.conocimiento_ID as ID, 
                    c.Nombre as Nombre,
                    c.Demanda as Demanda,
                    ac.IDAsesor,                    
                    FROM Asesor as a 
					Inner JOIN AsesorConocimiento as ac ON (a.IDAsesor=ac.IDAsesor)
					Inner JOIN Conocimiento as c ON (ac.conocimiento_ID = c.conocimiento_ID)
                    WHERE a.IDAsesor = @asesor  AND c.conocimiento_ID= @id",
                    new { id = id, asesor = asesor }
                    ).FirstOrDefault();

            }
        }

        public AsesorQueryModel GetAsesor(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<AsesorQueryModel>(
                    @"
                    SELECT a.IDAsesor as ID, 
                    a.Nombre as Nombre,
                    a.Apellido as Apellido,
                    a.Disponibilidad as Disponibilidad,
					a.Idioma as Idioma,
					a.Pais as Pais,
                    FROM Asesor as a
                    WHERE IDAsesor = @id                    
                    ", new { id = id }
                    ).FirstOrDefault();
            }
        }

        public List<AsesorQueryModel> ListAsesor()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<AsesorQueryModel>(
                    @"
                    SELECT a.IDAsesor as ID, 
                    a.Nombre as Nombre,
                    a.Apellido as Apellido,
                    a.Disponibilidad as Disponibilidad,
					a.Idioma as Idioma,
					a.Pais as Pais,
                    FROM Asesor as a
                    "
                    ).ToList();
            }
        }

        public List<AsesorQueryModel> ListAsesorConocimiento(int id)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                return connection
                    .Query<AsesorQueryModel>(
                    @"
                    SELECT c.conocimiento_ID as ID, 
                    c.Nombre as Nombre,
                    c.Demanda as Demanda,
					a.IDAsesor as IDAsesor,
                    FROM Asesor as a

                    Inner JOIN AsesorConocimiento ac ON(a.IDAsesor = ac.IDAsesor)

                    Inner JOIN Conocimiento as c ON(ac.conocimiento_ID = c.conocimiento_ID)
                    WHERE a.IDAsesor = @id").ToList();
            }
        }
    }
}
