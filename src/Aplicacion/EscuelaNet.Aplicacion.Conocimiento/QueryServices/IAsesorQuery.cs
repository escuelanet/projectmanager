﻿using EscuelaNet.Aplicacion.Conocimiento.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Conocimiento.QueryServices
{
    public interface IAsesorQuery
    {
        AsesorQueryModel GetAsesor(int id);
        List<AsesorQueryModel> ListAsesor();
        List<AsesorQueryModel> ListAsesorConocimiento(int id);
        AsesorQueryModel EncontrarConocimientoAsesor(int id, int asesor);

    }
}
