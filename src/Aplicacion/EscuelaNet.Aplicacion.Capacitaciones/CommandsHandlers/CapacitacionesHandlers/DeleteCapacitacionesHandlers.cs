﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.Commands.CapacitacionCommand;
using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;

namespace EscuelaNet.Aplicacion.Capacitaciones.CommandsHandlers.CapacitacionesHandlers
{
    public class DeleteCapacitacionesHandlers : IRequestHandler<DeleteCapacitacionCommand, CommandRespond>
    {
        private ICapacitacionRepository _capacitacionRepository;

        public DeleteCapacitacionesHandlers(ICapacitacionRepository capacitacionRepository)
        {
            _capacitacionRepository = capacitacionRepository;
        }

        public Task<CommandRespond> Handle(DeleteCapacitacionCommand request, CancellationToken cancellationToken)
        {
            var responde = new CommandRespond();

            var capa = _capacitacionRepository.GetCapacitacion(request.Id);
            _capacitacionRepository.Delete(capa);
            _capacitacionRepository.UnitOfWork.SaveChanges();

            responde.Succes = true;
            return Task.FromResult(responde);
        }
    }
}
