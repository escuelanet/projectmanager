﻿using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryServices
{
    public interface ITemasQuery
    {
        TemaQueryModel GetTema(int id);
        List<TemaQueryModel> ListTemas();
        List<TemaQueryModel> ListTemasInstructor(int id);

        List<TemaQueryModel> ListTemasCapacitacion(int id);
    }
}
