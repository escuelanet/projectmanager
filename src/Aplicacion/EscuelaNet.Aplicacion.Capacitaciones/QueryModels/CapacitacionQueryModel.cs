﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EscuelaNet.Aplicacion.Capacitaciones.QueryServices.CapacitacionServices;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Aplicacion.Capacitaciones.QueryModels
{
    public class CapacitacionQueryModel
    {
        public int Id { get; set; }

        
        public int IDLugar { get; set; }
        public Lugar Lugar { get; set; }
        public int Minimo { get; set; }
        public int Maximo { get; set; }
        public int Duracion { get; set; }
        public decimal Precio { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }

        public EstadoDeCapacitacion Estado { get; set; }
        //public IList<Alumno> Alumnos { get; set; }
        //public IList<Tema> Temas { get; set; }
        //public IList<Instructor> Instructores { get; set; }
        public string Calle { get; set; }
        public string Numero { get; set; }
        public string Depto { get; set; }
        public string Piso { get; set; }
    }
}
