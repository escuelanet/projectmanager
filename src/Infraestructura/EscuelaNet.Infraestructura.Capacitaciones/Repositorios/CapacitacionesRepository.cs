﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Infraestructura.Capacitaciones.Repositorios
{
    public class CapacitacionesRepository : ICapacitacionRepository
    {
        private CapacitacionContext _contexto;

        public CapacitacionesRepository(CapacitacionContext contexto)
        {
            _contexto = contexto;
        }
        public IUnitOfWork UnitOfWork => _contexto;
        public Dominio.Capacitaciones.Capacitacion Add(Dominio.Capacitaciones.Capacitacion capacitacion)
        {
            _contexto.Capacitaciones.Add(capacitacion);
            return capacitacion;
        }

        public void Delete(Dominio.Capacitaciones.Capacitacion capacitacion)
        {
            _contexto.Capacitaciones.Remove(capacitacion);
        }

        public Dominio.Capacitaciones.Capacitacion GetCapacitacion(int id)
        {
            var capacitacion = _contexto.Capacitaciones.Find(id);
            if (capacitacion != null)
            {
                _contexto.Entry(capacitacion)
                    .Collection(i => i.Temas).Load();

            }

            return capacitacion;
        }

        public List<Dominio.Capacitaciones.Capacitacion> ListCapacitaciones()
        {
            return _contexto.Capacitaciones.ToList();
        }

        public void Update(Dominio.Capacitaciones.Capacitacion capacitacion)
        {
            _contexto.Entry(capacitacion).State = EntityState.Modified;
        }
    }
}
