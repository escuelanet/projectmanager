CREATE TABLE [dbo].[Instructores]
(
    [IDInstructor] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
   [Nombre] VARCHAR(128) NOT NULL,
   [Apellido] VARCHAR(128) NOT NULL,
   [Dni] VARCHAR(128) NOT NULL,
   [FechaNacimiento] DATETIME  NOT NULL
)