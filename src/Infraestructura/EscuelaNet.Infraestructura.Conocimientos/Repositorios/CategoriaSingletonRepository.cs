﻿using EscuelaNet.Dominio.Conocimientos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos.Repositorios
{
    public class CategoriaSingletonRepository: ICategoriaRepository
    {
        private CategoriaSingleton contexto = CategoriaSingleton.Instancia;
        public IUnitOfWork UnitOfWork => contexto;

        public Categoria Add(Categoria categoria)
        {
            contexto.Categorias.Add(categoria);
            return categoria;
        }

        //public Asesor AddAsesor(Conocimiento conocimiento, Asesor asesor)
        //{
        //    contexto.Entry(asesor).State = EntityState.Unchanged;
        //    contexto.Entry(conocimiento.Categoria).State = EntityState.Unchanged;

        //    try
        //    {
        //        conocimiento.AgregarAsesor(asesor);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return asesor;
        //}

        public void Delete(Categoria categoria)
        {
            contexto.Categorias.Remove(categoria);
        }

        //public void DeleteAsesor(Conocimiento conocimiento, Asesor asesor)
        //{
        //    contexto.Entry(asesor).State = EntityState.Unchanged;
        //    contexto.Entry(conocimiento.Categoria).State = EntityState.Unchanged;
        //    try
        //    {
        //        conocimiento.AgregarAsesor(asesor);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void DeleteConocimiento(Conocimiento conocimiento)
        //{
        //    contexto.Conocimientos.Remove(conocimiento);
        //}

        public Categoria GetCategoria(int id)
        {
            return contexto.Categorias[id];
        }

        //public Conocimiento GetConocimieto(int id)
        //{
        //    var conocimiento = contexto.Conocimientos.Find(id);
        //    if (conocimiento != null)
        //    {
        //        contexto.Entry(conocimiento).Reference(con => con.Categoria).Load();
        //        contexto.Entry(conocimiento).Reference(con => con.Asesores).Load();
        //    }
        //    return conocimiento;
        //}

        public List<Categoria> ListCategoria()
        {
            return contexto.Categorias.ToList();
        }

        public void Update(Categoria categoria)
        {
            contexto.Categorias.RemoveAt(categoria.ID);
            contexto.Categorias.Insert(categoria.ID, categoria);
        }

    }
}
