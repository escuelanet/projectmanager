﻿using EscuelaNet.Dominio.Conocimientos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos.Repositorios
{
    public class AsesorRepositorio : IAsesorRepository
    {
        private CategoriaContext _context = new CategoriaContext();
        public IUnitOfWork UnitOfWork => _context;

        public AsesorRepositorio(CategoriaContext contexto)
        {
            _context = contexto;
        }

        public Asesor Add(Asesor asesor)
        {
            _context.Asesores.Add(asesor);
            return asesor;
        }

        public Conocimiento AddConocimiento(Conocimiento conocimiento, Asesor asesor)
        {
            _context.Entry(asesor).State = EntityState.Unchanged;
            _context.Entry(conocimiento.Categoria).State = EntityState.Unchanged;
            try
            {
                asesor.pushConocimiento(conocimiento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return conocimiento;
        }

        public void Delete(Asesor asesor)
        {
            _context.Asesores.Remove(asesor);
        }

        public void DeleteConocimiento(Conocimiento conocimiento, Asesor asesor)
        {
            try
            {
                asesor.pullConocimiento(conocimiento);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public Asesor GetAsesor(int id)
        {
            var asesor = _context.Asesores.Find(id);
            if (asesor != null)
            {
                _context.Entry(asesor).Collection(a => a.Conocimientos).Load();
            }
            return asesor;
        }

        public List<Asesor> ListAsesor()
        {
            return _context.Asesores.ToList();
        }

        public void Update(Asesor asesor)
        {
            _context.Entry(asesor).State = EntityState.Modified;
        }
    }
}
