﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{    public class Conocimiento : Entity
    {
        public string Nombre { get; set; }
        public Demanda Demanda { get; set; }
        public IList<Asesor> Asesores { get; set; }
        public IList<ConocimientoPorPrecio> ConocimientoPorprecio { set; get; }
        public int IDCategoria { get; set; }
        public Categoria Categoria { get; set; }

        public Conocimiento(string nombre)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            
        }
        private Conocimiento() { }

        public void CambiarDemanda(Demanda demanda)
        {
            if (this.Demanda == Demanda.Alta)
            {
                this.Demanda = demanda;
            }
            else if (this.Demanda == Demanda.Baja && demanda != Demanda.Alta)
            {
                this.Demanda = demanda;
            }
        }

        public void AgregarConocimientoPorPrecio(Nivel nivel, Double valorNominal, string moneda )
        {
            if (ConocimientoPorprecio == null)
            {
                ConocimientoPorprecio = new List<ConocimientoPorPrecio>();
            }
            ConocimientoPorprecio.Add(new ConocimientoPorPrecio
            {
                ValorNominal = valorNominal,
                Moneda = moneda,
                Nivel = nivel,
            });
        }

        public void AgregarAsesor(Asesor asesor)
        {
            if (Asesores == null)
            {
                Asesores = new List<Asesor>();
            }
            this.Asesores.Add(asesor);

        }
    }
}

