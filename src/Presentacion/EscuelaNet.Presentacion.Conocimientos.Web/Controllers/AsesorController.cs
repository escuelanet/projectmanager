﻿//using EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura;
using EscuelaNet.Presentacion.Conocimientos.Web.Models;
using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EscuelaNet.Infraestructura.Conocimientos.Repositorios;

namespace EscuelaNet.Presentacion.Categorias.Web.Controllers
{
    public class AsesorController : Controller
    {
        private IAsesorRepository _asesorrepository;
        private ICategoriaRepository _categoriarepository;
        public ActionResult Index()
        {
            var asesor = _asesorrepository.ListAsesor();

            var model = new AsesorIndexModel()
            {
                Titulo = "Asesores",
                Asesores = asesor
            };

            return View(model);
        }

        public ActionResult New()
        {
            var model = new NuevoAsesorModel()
            {
                Titulo = "Nuevo Asesor"
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevoAsesorModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var asesor = new Asesor(model.Nombre, model.Apellido, model.Idioma, model.Pais);
                    _asesorrepository.Add(asesor);
                    _asesorrepository.UnitOfWork.SaveChanges();

                    TempData["success"] = "¡Nuevo asesor creado correctamente!";
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio, ingrese todos los datos solicitados";
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var asesor = _asesorrepository.GetAsesor(id);
                var model = new NuevoAsesorModel()
                {
                    Titulo = "Editar el Asesor: " + asesor.Nombre + asesor.Apellido,
                    Nombre = asesor.Nombre,
                    Apellido = asesor.Apellido,
                    Disponibilidad = asesor.Disponibilidad,
                    Idioma = asesor.Idioma,
                    Pais = asesor.Pais,
                    IdAs=id
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevoAsesorModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var asesor = _asesorrepository.GetAsesor(model.IdAs);
                    asesor.Nombre = model.Nombre;
                    asesor.Apellido = model.Apellido;
                    asesor.Disponibilidad = model.Disponibilidad;
                    asesor.Idioma = model.Idioma;
                    asesor.Pais = model.Pais;

                    _asesorrepository.Update(asesor);
                    _asesorrepository.UnitOfWork.SaveChanges();

                    TempData["success"] = "¡Conocimiento Modificado Correctamente!";
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    TempData["error"] = e.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var asesor = _asesorrepository.GetAsesor(id);

                var model = new NuevoAsesorModel()
                {
                    Nombre = asesor.Nombre,
                    Apellido = asesor.Apellido,
                    Idioma = asesor.Idioma,
                    Pais = asesor.Pais,
                    IdAs = id,
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevoAsesorModel model)
        {
            try
            {
                var asesor = _asesorrepository.GetAsesor(model.IdAs);
                _asesorrepository.Delete(asesor);
                _asesorrepository.UnitOfWork.SaveChanges();

                TempData["success"] = "¡Asesor borrado correctamente!";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }

        //public ActionResult Conocimiento(int id)
        //{
        //    var asesor = _asesorrepository.GetAsesor(id);
        //    var model = new AsesorConocimientoModel()
        //    {
        //        Asesor = asesor,
        //        Conocimientos = asesor.Conocimientos.ToList()
        //    };
        //    return View(model);
        //}
        public ActionResult DeleteConocimmiento(int id, int asesor)
        {
            var asesorBuscado = _asesorrepository.GetAsesor(asesor);
            var conocimientobuscado = asesorBuscado.Conocimientos.First(c => c.ID == id);
            var model = new NuevoAsesorConocimientoModel()
            {
                NombreConocimiento = conocimientobuscado.Nombre + " " + conocimientobuscado.Demanda,
                IDAsesor = asesor,
                NombreAsesor = asesorBuscado.Nombre + " " + asesorBuscado.Apellido,
                IDConocimiento = id,
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteConocimmiento(NuevoAsesorConocimientoModel model)
        {
            try
            {
                var asesorBuscado = _asesorrepository.GetAsesor(model.IDAsesor);
                var conocimientobuscado = asesorBuscado.Conocimientos.First(c => c.ID == model.IDConocimiento);
                asesorBuscado.pullConocimiento(conocimientobuscado);

                _asesorrepository.Update(asesorBuscado);
                _asesorrepository.UnitOfWork.SaveChanges();

                TempData["success"] = "¡Asesor borrado correctamente!";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
        public ActionResult NewConocimiento(int id)
        {
            var asesor = _asesorrepository.GetAsesor(id);
            var categorias = _categoriarepository.ListCategoria();
            var conocimientos = new List<Conocimiento>();

            foreach(var categoria in categorias)
            {
                foreach (var conocimientoEnCategoria in categoria.Conocimientos)
                {
                    conocimientos.Add(conocimientoEnCategoria);
                };
            }
            var model = new NuevoAsesorConocimientoModel()
            {
                IDAsesor = id,
                NombreAsesor = asesor.Nombre,
                Conocimientos = conocimientos
            };
            return View(model);
        }

        //[HttpPost]
        //public ActionResult NewConocimiento(NuevoAsesorConocimientoModel model)
        //{
        //    try
        //    {
        //        var asesorBuscado = _asesorrepository.GetAsesor(model.IDAsesor);
        //        var conocimientoBuscado = _categoriarepository.GetConocimieto(model.IDConocimiento);

        //        var bandera = true;
        //        var bandera2 = true;

        //        foreach (var conocimientoencontrado in asesorBuscado.Conocimientos)
        //        {
        //            if (conocimientoencontrado.IDCategoria != conocimientoBuscado.Categoria.ID)
        //            {
        //                bandera = false;
        //            }
        //            if (conocimientoencontrado.ID == model.IDConocimiento)
        //            {
        //                bandera2 = false;
        //            }
        //        }
        //        if (bandera && bandera2)
        //        {
        //            _categoriarepository.AddAsesor(conocimientoBuscado, asesorBuscado);
        //            _categoriarepository.Update(conocimientoBuscado.Categoria);
        //            _categoriarepository.UnitOfWork.SaveChanges();

        //            TempData["success"] = "¡Asesor Creado Correctamente!";
        //            return RedirectToAction("../Asesor/Conocimiento/" + model.IDAsesor);
        //        }
        //        else if (bandera2)
        //        {
        //            TempData["error"] = "" + "¡ERROR! El asesor fue vinculado a un conocimiento de otra categoria";
        //            return RedirectToAction("../Asesor/Conocimiento/" + model.IDAsesor);
        //        }
        //        else
        //        {
        //            TempData["error"] = "¡ERROR! El asesor ya fue vinculado a este conocimiento";
        //            return RedirectToAction("../Asesor/Conocimiento/" + model.IDAsesor);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["error"] = ex.Message;
        //        return RedirectToAction("../Asesor/Conocimiento/" + model.IDAsesor);
        //    }

            //    asesorBuscado.pushConocimiento(conocimientoBuscado);
            //    repositorio.Update(asesorBuscado);
            //    repositorio.UnitOfWork.SaveChanges();

            //    TempData["success"] = "Asesor agregado correctamente";
            //    return RedirectToAction("Index");
            //}
            //catch (Exception ex)
            //{
            //    TempData["error"] = ex.Message;
            //    return View(model);
        
    }
}

