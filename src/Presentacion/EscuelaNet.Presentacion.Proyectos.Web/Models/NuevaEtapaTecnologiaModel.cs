﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Proyectos.Web.Models
{
    public class NuevaEtapaTecnologiaModel
    {
        public int IDLinea { get; set; }
        public int IDProy { get; set; }
        public int IDEtapa { get; set; }
        public int IDTecno { get; set; }
        public string NombreTecnologia { get; set; }
        public List<Tecnologias> Tecnologias { get; set; }
    }
}