﻿using EscuelaNet.Aplicacion.Capacitaciones.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class CapacitacionIndexModel
    {
        public string Titulo { get; set; }

        public List<CapacitacionQueryModel> Capacitaciones { get; set; }
    }
}